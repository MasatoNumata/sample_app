class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  include SessionsHelper
  # この親クラスでモジュールをincludeすることで、ヘルパーを全コントローラで使えるようになる。
end
